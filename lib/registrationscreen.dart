import 'dart:developer';
import 'dart:html';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'main.dart';

class ProfileScreen extends StatefulWidget {
  const ProfileScreen({Key? key}) : super(key: key);

  @override
  State<ProfileScreen> createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Create Account"),
        centerTitle: true,
      ),
      body: UserForm(),
    );
  }
}

class UserForm extends StatefulWidget {
  const UserForm({Key? key}) : super(key: key);

  @override
  State<UserForm> createState() => _UserFormState();
}

class _UserFormState extends State<UserForm> {
  Widget _Space(double Height, double Width) {
    return SizedBox(
      height: Height,
      width: Width,
    );
  }

  @override
  Widget build(BuildContext context) {
    TextEditingController nameController = TextEditingController();
    TextEditingController emailController = TextEditingController();
    TextEditingController passwordController = TextEditingController();

    Future _addForm() {
      final fullName = nameController.text;
      final email = emailController.text;
      final password = passwordController.text;

      final ref =
          FirebaseFirestore.instance.collection("User Information").doc();
      return ref
          .set({
            "User Name": fullName,
            "User Email": email,
            "User Password": password,
            "doc_Id": ref.id
          })
          .then((value) => log("Collection Added!!"))
          .catchError((onError) => log(onError));
    }

    return Container(
        child: Container(
      child: Form(
        child: Column(
          children: <Widget>[
            _Space(100.0, 0.0),
            TextField(
              controller: nameController,
              decoration: const InputDecoration(
                border: OutlineInputBorder(),
                labelText: "Full Name",
                hintText: "Enter Full Name",
              ),
            ),
            _Space(20.0, 0.0),
            TextField(
              controller: emailController,
              decoration: const InputDecoration(
                border: OutlineInputBorder(),
                labelText: "Email",
                hintText: "Enter Email",
              ),
            ),
            _Space(20.0, 0.0),
            TextField(
                controller: passwordController,
                decoration: const InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: "Password",
                  hintText: "Enter Password",
                )),
            _Space(20.0, 0.0),
            ElevatedButton(
                onPressed: () {
                  _addForm();
                  Navigator.push(context,
                      MaterialPageRoute(builder: (BuildContext) {
                    return MyHomePage();
                  }));
                },
                child: const Text("Submit"))
          ],
        ),
      ),
    ));
  }
}
