import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';

class MyHomeworks extends StatefulWidget {
  const MyHomeworks({Key? key}) : super(key: key);

  @override
  State<MyHomeworks> createState() => _MyHomeworksState();
}

class _MyHomeworksState extends State<MyHomeworks> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('My Homeworks'),
        centerTitle: true,
      ),
      body: ListofHomework(),
    );
  }
}

class ListofHomework extends StatefulWidget {
  const ListofHomework({Key? key}) : super(key: key);

  @override
  State<ListofHomework> createState() => _ListofHomeworkState();
}

class _ListofHomeworkState extends State<ListofHomework> {
  final Stream<QuerySnapshot> _mysession =
      FirebaseFirestore.instance.collection('Homework').snapshots();

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
      stream: _mysession,
      builder: (BuildContext context,
          AsyncSnapshot<QuerySnapshot<Object?>> snapshot) {
        if (snapshot.hasError) {
          return const Text('Something went wrong');
        }
        if (snapshot.connectionState == ConnectionState.waiting) {
          return const Center(
            child: CircularProgressIndicator(),
          );
        }
        if (snapshot.hasData) {
          return Row(
            children: [
              Expanded(
                  child: SizedBox(
                      height: (MediaQuery.of(context).size.height),
                      width: MediaQuery.of(context).size.width,
                      child: ListView(
                        children: snapshot.data!.docs
                            .map((DocumentSnapshot document) {
                          Map<String, dynamic> data =
                              document.data()! as Map<String, dynamic>;
                          return ListTile(
                            title: Text(data['My Homework']),
                            subtitle: Row(
                              children: [
                                Text(data['Due Date']),
                                const SizedBox(
                                  width: 10.0,
                                ),
                                Text(data['doc_id'])
                              ],
                            ),
                          );
                        }).toList(),
                      )))
            ],
          );
        } else {
          return const Text('No Data');
        }
      },
    );
  }
}
