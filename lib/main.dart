import 'package:flutter/material.dart';
import 'package:flutter_application_module5/registrationscreen.dart';
import 'registrationscreen.dart';
import 'firstscreen.dart';
import 'package:firebase_core/firebase_core.dart';

Future main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
      options: const FirebaseOptions(
          apiKey: "AIzaSyBNsKUoAHWFDYtKp9OkMi2lKNRFON8pgN0",
          authDomain: "my-module-5-project.firebaseapp.com",
          projectId: "my-module-5-project",
          storageBucket: "my-module-5-project.appspot.com",
          messagingSenderId: "627212844480",
          appId: "1:627212844480:web:ad106dcb3520ab8ebcdc25",
          measurementId: "G-V6C7KFM0M0"));
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Module 5: Homework Calendar',
      theme: ThemeData(
        primarySwatch: Colors.brown,
      ),
      debugShowCheckedModeBanner: false,
      home: const MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key}) : super(key: key);

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Flutter Project Module 5"),
        centerTitle: true,
      ),
      body: LogIn(),
    );
  }
}

class LogIn extends StatefulWidget {
  const LogIn({Key? key}) : super(key: key);

  @override
  State<LogIn> createState() => _LogInState();
}

class _LogInState extends State<LogIn> {
  Widget _Space(double Height, double Width) {
    return SizedBox(
      height: Height,
      width: Width,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.all(25.0),
      child: Column(
        children: <Widget>[
          _Space(200.0, 0.0),
          const TextField(
            decoration: InputDecoration(
              border: OutlineInputBorder(),
              labelText: "Email",
              hintText: "Enter Email",
            ),
          ),
          _Space(20.0, 0.0),
          const TextField(
            decoration: InputDecoration(
              border: OutlineInputBorder(),
              labelText: "Password",
              hintText: "Enter Password",
            ),
          ),
          _Space(20.0, 0.0),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              ElevatedButton(
                onPressed: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (BuildContext) {
                    return const FirstScreen();
                  }));
                },
                child: const Text("LogIn"),
              ),
              _Space(0.0, 20.0),
              ElevatedButton(
                onPressed: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (BuildContext) {
                    return ProfileScreen();
                  }));
                },
                child: const Text("Register"),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
