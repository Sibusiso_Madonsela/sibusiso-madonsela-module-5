import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';

class EditScreen extends StatefulWidget {
  const EditScreen({Key? key}) : super(key: key);

  @override
  State<EditScreen> createState() => _EditScreenState();
}

class _EditScreenState extends State<EditScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Edit'),
        centerTitle: true,
      ),
      body: SafeArea(child: EditField()),
    );
  }
}

class EditField extends StatefulWidget {
  const EditField({Key? key}) : super(key: key);

  @override
  State<EditField> createState() => _EditFieldState();
}

class _EditFieldState extends State<EditField> {
  Widget _Space(double Height, double Width) {
    return SizedBox(
      height: Height,
      width: Width,
    );
  }

  @override
  Widget build(BuildContext context) {
    TextEditingController _homeworkController = TextEditingController();
    TextEditingController _idController = TextEditingController();
    TextEditingController _submissionController = TextEditingController();

    Future _UpdateDelete() {
      final homeworkUpdate = _homeworkController.text;
      final submissionUpdate = _submissionController.text;
      final idUpdateDelete = _idController.text;
      String doc_id = _idController.text;

      final ref = FirebaseFirestore.instance.collection('Homework').doc(doc_id);

      return ref.update({
        "My Homework": homeworkUpdate,
        "Due Date": submissionUpdate,
        //"doc_id": ref.id
      });
    }

    Future _Delete() {
      // final homeworkUpdate = _homeworkController.text;
      // final submissionUpdate = _submissionController.text;
      // final idUpdateDelete = _idController.text;
      String doc_id = _idController.text;

      final ref = FirebaseFirestore.instance.collection('Homework').doc(doc_id);

      return ref.delete();
    }

    return Container(
      margin: const EdgeInsets.all(25.0),
      //alignment: Alignment.center,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          _Space(20.0, 0.0),
          TextField(
            controller: _homeworkController,
            decoration: const InputDecoration(
              border: OutlineInputBorder(),
              hintText: 'Enter Homework',
            ),
          ),
          _Space(20.0, 0.0),
          TextField(
            controller: _submissionController,
            decoration: const InputDecoration(
              border: OutlineInputBorder(),
              hintText: 'Due Date',
            ),
          ),
          _Space(20.0, 0.0),
          TextField(
            controller: _idController,
            decoration: const InputDecoration(
              border: OutlineInputBorder(),
              hintText: 'Document Id',
            ),
          ),
          _Space(20.0, 0.0),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              ElevatedButton(
                  onPressed: () {
                    _UpdateDelete();
                  },
                  child: const Text('Update')),
              _Space(0.0, 20.0),
              ElevatedButton(
                  onPressed: () {
                    _Delete();
                  },
                  child: const Text('Delete')),
            ],
          )
        ],
      ),
    );
  }
}
