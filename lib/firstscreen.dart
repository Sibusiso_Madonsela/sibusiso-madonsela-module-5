import 'dart:developer';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';

import 'dart:html';
import 'editscreen.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter_application_module5/editscreen.dart';
import 'package:flutter_application_module5/secondscreen.dart';

class FirstScreen extends StatefulWidget {
  const FirstScreen({Key? key}) : super(key: key);

  @override
  State<FirstScreen> createState() => _FirstScreenState();
}

class _FirstScreenState extends State<FirstScreen> {
  Widget _Space(double Height, double Width) {
    return SizedBox(
      height: Height,
      width: Width,
    );
  }

  @override
  Widget build(BuildContext context) {
    TextEditingController homeworkController = TextEditingController();
    TextEditingController submissionController = TextEditingController();

    Future _AddingHomework() {
      final homework = homeworkController.text;
      final submission = submissionController.text;

      final ref = FirebaseFirestore.instance.collection("Homework").doc();
      return ref
          .set({
            "My Homework": homework,
            "Due Date": submission,
            "doc_id": ref.id
          })
          .then((value) => log("Homework was added"))
          .catchError((onError) => log(onError));
    }

    return Scaffold(
      appBar: AppBar(
        title: const Text("Dashboard"),
      ),
      body: Container(
        margin: const EdgeInsets.all(20.0),
        child: Form(
            child: Column(
          children: <Widget>[
            _Space(200.0, 0.0),
            TextField(
              controller: homeworkController,
              decoration: const InputDecoration(
                  border: OutlineInputBorder(), hintText: "Enter Homewok"),
            ),
            _Space(20.0, 0.0),
            TextField(
              controller: submissionController,
              decoration: const InputDecoration(
                border: OutlineInputBorder(),
                hintText: "Enter Due Date",
              ),
            ),
            _Space(20.0, 0.0),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                ElevatedButton(
                    onPressed: () {
                      _AddingHomework();
                    },
                    child: const Text("Add Homework")),
                _Space(0.0, 20.0),
                ElevatedButton(
                    onPressed: () {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (BuildContext) {
                        return MyHomeworks();
                      }));
                    },
                    child: const Text("View Homework")),
              ],
            ),
          ],
        )),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.push(context, MaterialPageRoute(builder: (BuildContext) {
            return EditScreen();
          }));
        },
        elevation: 5.0,
        hoverElevation: 20.0,
        child: const Text('Edit'),
      ),
    );
  }
}
